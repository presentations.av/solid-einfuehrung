/* eslint-disable import/no-webpack-loader-syntax */

// Import React
import React from 'react'
// Import Spectacle Core tags
import {
  Anim,
  Appear,
  BlockQuote,
  Cite,
  CodePane,
  Deck,
  Heading,
  Image,
  Link,
  List,
  ListItem,
  Notes,
  Quote,
  Slide,
  Text,
} from 'spectacle'

import {LiveEditor, LiveError, LivePreview, LiveProvider} from 'react-live'

import {Image as SolidImage, Link as SolidLink, List as SolidList, Name, Value} from '@solid/react'
// Import theme
import createTheme from 'spectacle/lib/themes/default'

import flame from './flame.module.css'

const URL = ({href, small, ...props}) => <Link textSize={small ? 20 : undefined} {...props} href={href}>{href}</Link>;

// Require CSS
require('normalize.css');

const theme = createTheme(
  {
    primary: 'white',
    secondary: '#1F2022',
    tertiary: '#7C4DFF',
    quaternary: '#18A9E6',
    source: '#666'
  },
  {
    primary: {
      name: 'Roboto',
      googleFont: true,
      styles: ['400', '700i']
    },
    secondary: 'Helvetica',
  }
);

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck
        transition={['fade']}
        transitionDuration={500}
        theme={theme}
      >
        <Slide>
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            Die Rückeroberung des
          </Heading>
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            Social Web
          </Heading>
          <Text margin="10px 0 0" textColor="tertiary" size={1} fit bold>
            Eine Einführung in Solid
          </Text>
          <Notes>
            <List>
              <ListItem>2018: Tim Berners Lee geht mit Solid an die Öffentlichkeit</ListItem>
              <ListItem>Missstände im zentralisierten  Social Web beheben</ListItem>
              <ListItem>Nutzer:innen wieder in den Mittelpunkt stellen</ListItem>
              <ListItem>Was ist Solid? Wie erobern wir das Social Web zurück</ListItem>
            </List>

          </Notes>
        </Slide>

        <Slide>
          <div style={{display: 'flex'}}>
            <SolidImage style={{borderRadius: 50}} src="[https://angelo.veltens.org/profile/card#me].image"/>

            <div>
              <Heading textColor="tertiary" size={4}><Value src="[https://angelo.veltens.org/profile/card#me].name"/></Heading>
              <Heading size={6}><Value
                src="[https://angelo.veltens.org/profile/card#me].vcard_role"/></Heading>
              <URL href="https://angelo.veltens.org/profile/card#me" small style={{marginTop: '1rem'}} />
            </div>
          </div>
          <Notes>
            Alle Daten auf dieser Folien werden live aus meinen Solid Profil geladen.
            Solid gibt mir volle Kontrolle über meine Daten.
            Ich kann diese auf jeder beliebigen Webseite einbinden und jeder App zugänglich machen.
          </Notes>
        </Slide>

        <Slide>
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            Was haben wir
          </Heading>
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            verloren?
          </Heading>
          <Text margin="10px 0 0" textColor="tertiary" size={1} fit bold>
            Eine kurze Geschichte des Social Web
          </Text>
          <Notes>
            Bevor ich näher auf Solid eingehe, möchte ich zunächst erklären, was es denn eigentlich rückzuerobern gilt.
            Was haben wir verloren und wie konnte es soweit kommen?
          </Notes>
        </Slide>

        <Slide bgImage="./images/website.png">
          <Notes>
            Das Web war eigentlich schon immer ein sozialer Ort. Menschen veröffentlichen
            Informationen über sich, ihre Arbeit, ihre Interessen, gemeinsame Veranstaltungen
            auf Seiten im Web.
            Über Links stoße ich auf verwandte Informationen und lerne neue Dinge kennen.
            Jeder kann mit ein wenig HTML an diesem Web teilnehmen,
            aber es gibt eine gewissen Einstiegshürde.
          </Notes>
        </Slide>

        <Slide bgImage="./images/web20.png">
          <Notes>
            Mit dem Web 2.0 wurde das Web interaktiver und leichter zugänglich für Nicht-Techies.
            Eine Fülle von Foren und Blogs und zahlreiche interaktive Dienste entstanden.
            Während Inhalte zu veröffentlichen einfacher wurde, wurde der Betrieb von Webseiten,
            die jetzt eher Webanwendungen waren komplizierter.
          </Notes>
        </Slide>

        <Slide bgImage="./images/social_media.jpg" align="flex-start flex-end">
          <span style={{background: 'white', color: 'black'}}>
            Hintergrundbild von <a href="https://www.flickr.com/photos/nickrate/">nickrate</a> - <a
            href="https://creativecommons.org/licenses/by/2.0/">cc-by 2.0</a>
          </span>
          <Notes>
            Eine Fülle an Social Media Diensten entstand und brachte zahlreiche neue Funktionen ins Web und machte
            altbekanntes einfacher.
            Statt etwas selbst zu hosten, kann ich einfach interaktive Dienste nutzen. Im Gegensatz zum offenen Web
            basieren die Dienste aber darauf die Daten in sich einzuschließen statt zu verlinken.
            Konkurrenz und Ausschluss statt Kooperation und Verlinkung.
          </Notes>
        </Slide>

        <Slide bgImage="./images/konsolidierung.svg" bgSize="50%" bgRepeat="no-repeat" align="flex-start flex-start">
          <Heading style={{padding: "1rem"}} size={1} fit caps lineHeight={1}>
          Konsolidierung</Heading>
          <Notes>
            Darauf folgte zwangsläufig eine Konsolidierung. Netzwerkeffekte treten in Kraft.
            Niemand kann bei allen Platformen sein. Es macht Sinn, die Dienste zu nutzen, die schon
            Freund:innen nutzen. -> Zentralisierung
            Platformen wie Facebook beginnen alles im Web zu vereinnahmen.
            Es wird unmöglich mit diesen zu konkurrieren. Mitbewerben fehlt dazu die Nutzer- und Datenbasis.
            Klassische Blogs und Webseiten können ohne Social Media Funktionen keine nennenswerte Reichweite
            erzielen.
            Warum überhaupt ein Blog betreiben, wenn man auch direkt auf die Platform posten kann?
          </Notes>
        </Slide>



        <Slide>
          <div style={{display: 'flex'}}>
            <Image src='./images/self-hosted.png' height="90vh"/>
            <div>
              <Heading>
                Awesome self-hosted
              </Heading>
              <URL
                href={'https://github.com/Kickball/awesome-selfhosted'} />
            </div>
          </div>
          <Notes>
            Man konnte natürlich versuchen mit selbstgehosteter Open Source Software von den Platformen unabhängig zu bleiben.
            Dazu müsste man jedoch immer mehr Dienste selbst hosten.
            Die awesome-self-hosted Liste auf github ist so lang, dass ich sie auf 58 Pixel Breite hätte schrumpfen
            müssen um sie vollständig auf diese Folie zu bringen. Hier ist daher nur das Inhaltsverzeichnis mit
            Kategorien(!) von Software die man potentiell selbst hosten könnte.
            Selbst-Hosting wird zur Mammutaufgabe.
          </Notes>
        </Slide>

        <Slide bgImage="./images/hell.jpg">
          <Heading textColor="primary" style={{padding: '1rem'}} className={flame.flame}>Self-hosting Hölle</Heading>
          <Appear>
            <div style={{display: 'flex'}}>
              <List className={flame.flame} bulletStyle=" ">
                <ListItem textSize={80}>Blog</ListItem>
                <ListItem textSize={80}>Kontakte</ListItem>
                <ListItem textSize={80}>Kalender</ListItem>
                <ListItem textSize={80}>Notizen</ListItem>
                <ListItem textSize={80}>Fotos</ListItem>
                <ListItem textSize={80}>Dateien</ListItem>
                <ListItem textSize={80}>...</ListItem>
              </List>
              <Appear>
                <List className={flame.flame} bulletStyle=" ">
                  <ListItem textSize={80}>Installation</ListItem>
                  <ListItem textSize={80}>Updates</ListItem>
                  <ListItem textSize={80}>Bugs</ListItem>
                  <ListItem textSize={80}>Downtimes</ListItem>
                  <ListItem textSize={80}>Backups</ListItem>
                  <ListItem textSize={80}>Security</ListItem>
                  <ListItem textSize={80}>...</ListItem>
                </List>
              </Appear>
            </div>
          </Appear>

          <Notes>
            Selbst-Hosting wird zur Hölle. Selbst wenn man nur die wichtigsten Social Media Dienste
            selbst betreiben will hat man viel zu tun: Blog, Website, Kontakte, Kalender, Notizen, Dateifreigaben.
            Hinzu kommen etliche komplexe IT Probleme: Alles muss installiert und aktualisiert werden.
            Backups müssen gemacht, mit Bugs und Downtimes muss irgendwie umgegangen werden.
            Und die ganze Mühe bringt nicht viel, wenn das soziale Umfeld nur über die geschlossenen Platformen erreichbar ist.
            Am Ende gaben wir uns also der Verlockung der Platformen hin.
          </Notes>
        </Slide>
        <Slide bgImage="./images/platforms.jpg" bgSize="80%">
          <Notes>
            Wir haben uns daran gewöhnt in Platformen zu denken.
            Wir sind gar nicht mehr im Web unterwegs.
            Sondern auf Twitter. Auf Instagram. Auf Facebook.
            Selbst gebloggt wird auf zentralen Platformen wie Medium.
          </Notes>
        </Slide>

        <Slide bgImage="./images/trapped.jpg">
          <Heading size={1} fit caps lineHeight={1} textColor="primary" >
            <div style={{ backgroundColor: 'rgba(0,0,0,0.8)', padding: '0.1rem'}}>Was haben wir verloren?</div>
          </Heading>
          <Appear>
            <Heading size={1} fit caps lineHeight={1} textColor="tertiary" >
              <div style={{ backgroundColor: 'rgba(0,0,0,0.8)'}}>Wahlfreiheit</div>
            </Heading>
          </Appear>
          <Notes>
            Doch dabei haben wir eine ganze Menge verloren.
            Das soziale Leben findet nicht mehr im Web sondern auf diesen Platformen statt.
            Wenn wir daran teilhaben wollen müssen wir uns den Regeln dieser Platformen beugen.

            Wir haben diesen Platformen die totale Kontrolle über unsere Inhalte und die Art und Weise wie wir
            und vernetzen können übertragen.

            Sie entscheiden über Priorisierung unserer Inhalte und welche Funktionen es gibt.
            Die Platform betrachtet unsere Daten als ihre. Sie stiehlt uns die Freiheit damit zu
            tun was wir möchten und nimmt sich selbst die heraus die Daten für alle
            möglichen Zwecke zu missbrauchen.

            Wir haben unsere Wahlfreiheit verloren und müssen uns den Regeln der Platform beugen.
            Uns damit abfinden was möglich ist und was nicht.
          </Notes>
        </Slide>

        <Slide bgColor="secondary">
          <BlockQuote>
            <Quote>I’ve always believed the web is for everyone. That’s why I
              and others fight fiercely to protect it</Quote>
            <Cite textColor="quaternary">Tim Berners-Lee</Cite>
          </BlockQuote>
          <Notes>
            Zum Glück ist aber noch nicht alles verloren. Denn das Web von damals existiert noch.
            Sein Erfinder TimBL sagte folgendes.
            Solid ist Teil dieser Mission das Web zu schützen
            und (wieder) allen zugänglich zu machen.
          </Notes>
        </Slide>
        <Slide bgColor="secondary">
          <BlockQuote>
            <Quote textSize={40}>Solid is how we <span>
              <Anim
                order={1}
                transitionDuration={300}
                fromStyle={{
                  color: 'primary',
                }}
                toStyle={[{
                  color: '#7C4DFF',
                }]}
                easing={'bounceOut'}
              ><span>evolve the web</span></Anim></span> in order to restore balance — by giving every one of us
              complete <span>
              <Anim
                order={1}
                transitionDuration={300}
                fromStyle={{
                  color: 'primary',
                }}
                toStyle={[{
                  color: '#7C4DFF',
                }]}
                easing={'bounceOut'}
              ><span>control over data</span></Anim></span>, personal or not, in a revolutionary
              way.</Quote>
            <Cite textColor="quaternary">Tim Berners-Lee</Cite>
            <Notes>
              <List>
                <ListItem>keine Anwendung, sondern Erweiterung des Webs</ListItem>
                <ListItem>es geht um die Kontrolle und Selbstbestimmung über unsere Daten</ListItem>
              </List>
            </Notes>
          </BlockQuote>
        </Slide>

        <Slide bgImage="./images/facebook.jpg" align="flex-start center">
          <Appear>
            <div style={{backgroundColor: 'rgba(0,0,0,0.8 )', padding: 10, alignItems: 'flex-start'}}>
              <Heading size={1} fit caps lineHeight={1} textColor="primary">
                Zentrales Datensilo
              </Heading>
              <Image src='./images/data-silo.svg'/>
            </div>
          </Appear>
          <Notes>
            Zentrale Platformen widersprechen diesem Gedanken des Webs.
            Es handelt sich um geschlossene Datensilos.
            Ein Austausch ist nur innerhalb des Silos möglich.
            Der Anbieter kontrolliert alles.
          </Notes>
        </Slide>

        <Slide bgImage="./images/diaspora.png" align="flex-start center">
          <Appear>
            <div style={{backgroundColor: 'rgba(0,0,0,0.8 )', padding: 10, alignItems: 'flex-start'}}>
              <Heading size={1} fit caps lineHeight={1} textColor="primary">
                Verteilte Datensilos
              </Heading>
              <Image src='./images/verteilte-datensilos.svg'/>
            </div>
          </Appear>
          <Notes>
            Mit Platformen wie Mastodon oder Diaspora gibt es Ansätze zur Dezentralisierung.
            Aber auch diese Anwendungen sind in sich geschlossene, wenn auch verteilte Datensilos.
            Austausch mit anderen Platformen ist nur eingeschränkt möglich.
            Daten müssen synchronisiert werden.
            Kopplung des Dienstes / der Anwendung mit den Daten. Mastodon = Twittern -> anderes nicht möglich.
          </Notes>
        </Slide>

        <Slide bgImage="./images/solid.svg" bgSize="50%" bgRepeat="no-repeat" align="flex-start flex-start">
            <Heading style={{paddingBottom: "1rem"}} size={1} fit caps lineHeight={1}>
              Social Linked Data (Solid)
            </Heading>
          <Notes>
            Bei Solid ist das anders:
            <List>
              <ListItem>Web aus Daten, statt verteilter Datensilos.</ListItem>
              <ListItem>granular verlink- und kombinierbar.</ListItem>
              <ListItem>Trennung von Apps und Daten.</ListItem>
              <ListItem>Alle möglichen Daten lassen im Web sich speichern</ListItem>
              <ListItem>Alle möglichen Anwendungen können diese potentiell Nutzen</ListItem>
              <ListItem>Eigene Daten mit bestehenden kombinieren</ListItem>
            </List>
          </Notes>
        </Slide>

        <Slide align="flex-start center" bgColor="quaternary" bgRepeat="no-repeat" bgSize="80%" bgImage="./images/differences.svg">
          <Notes>
            Es gibt also nicht nur zentral oder dezentral, sondern es gibt verschiedene Grade der Dezentralisierung.
            Angefangen von zentralen Diensten mit nur einem "Datentopf" für alle Nutzer:innen bis zu Solid mit einem oder sogar mehreren Datentöpfen pro User.
            Ein solcher Datentopf heißt bei Solid "POD".
          </Notes>
        </Slide>

        <Slide align="flex-start center" bgColor="tertiary" bgRepeat="no-repeat" bgSize="80%" bgImage="./images/data-aggregation.svg">
          <Notes>Schauen wir uns an, woher die Daten eines typischen Social Media Postings stammen, wenn wir die Solid Philosophie verfolgen. Janes Daten stammen aus Janes Pod(s). Jede einzelne Reaktion und jeder einzelne Kommentar stammt aus den Pods der jeweiligen Nutzer:innen. Die App die das Posting anzeigt folgt den einzelnen Links um die Daten zu aggregieren und darzustellen.</Notes>
        </Slide>

        <Slide align="flex-start center" bgColor="tertiary" bgRepeat="no-repeat" bgSize="90%" bgImage="./images/linked-data.svg">
          <Notes>Dies ist dadurch möglich, dass alles im Solid Web über eine eigene URI identifiert wird und referenzierbar ist. Anwendungen können den Links folgen und alle nötigen Daten aus dem Web zusammentragen</Notes>
        </Slide>


        <Slide align="flex-start center" bgColor="primary" bgRepeat="no-repeat" bgSize="80%" bgImage="./images/apps-daten-trennung.svg">
          <Notes>
            Mehrere Apps greifen auf die gleichen Daten zu und sind somit automatisch "in-sync".
            Gefällt mir eine App nicht mehr, kann ich diese wechseln, ohne meine Daten zu verlieren.
          </Notes>
        </Slide>

        <Slide>
          <CodePane textSize={30} source={require("!raw-loader!./assets/profile.example")} lang="js" />
          <Text>Dokument: https://angelo.veltens.org/profile/card</Text>
          <Notes>
            Wie sehen diese Daten aus? Hier ein Auszug aus meinem Solid Profil. Es ist nichts weiter als ein Dokument
            auf einem Solid Web Server. Dieses Dokument enthält strukturierte Daten über mich und Links auf weitere Daten.
          </Notes>
        </Slide>
        <Slide>
          <LiveProvider code={`({me = "https://angelo.veltens.org/profile/card#me"}) => <>
     <Image
       src={\`[\${me}].image\`}
       style={{float: "left"}}
     />
     <h2><a href={me}><Name src={me}/></a></h2>
     <Value src={\`[\${me}].vcard_role\`}/>
     <p><Link href={\`[\${me}].foaf_homepage\`}>Homepage</Link></p>
</>`}
                        scope={{Value, List: SolidList, Image: SolidImage, Link: SolidLink, Name}}>

            <div style={{backgroundColor: '#111', borderRadius: '5px', caretColor: 'white', width: '100%', marginTop: '1rem',marginBottom: '1rem'}}>
              <LiveEditor/>
            </div>

            <div style={{backgroundColor: 'white', width: '100%',  borderRadius: '5px', padding:'1rem'}}>
              <LiveError/>
              <LivePreview/>
            </div>
          </LiveProvider>
          <Notes>
            Hier sehen wir, wie eine React-Anwendung auf die Daten meines Solid-Profils zugreifen kann.
            Homepage zeigt noch mehr Daten (Accounts, Bookmarks)
            Alle Daten sind über die WebID verlinkt (Demo: Profil Rohdaten)
            Demo: Freunde auflisten und dann z.B. die URI austauschen
          </Notes>
        </Slide>
        <Slide align="flex-start center" bgRepeat="no-repeat" bgSize="80%" bgImage="./images/standards.svg">
          <Notes>
            <List>
              <ListItem>Weiterentwicklung des Webs</ListItem>
              <ListItem>bewährte Webstandards + neue</ListItem>
              <ListItem>Community + Inrupt verwirklichen gemeinsame Vision</ListItem>
              <ListItem>In der Praxis: Server + Apps die die Solid-Standards implementieren</ListItem>
              <ListItem>Gleicher Ansatz wie beim klassischen Web</ListItem>
            </List>
          </Notes>
        </Slide>
        <Slide align="flex-start center" bgRepeat="no-repeat" bgSize="60%" bgImage="./images/flow.svg">
          <Notes>
            Hier sehen wir, wie der verschiedenen Standards in der Funktion von Solid Apps zusammenspielen.
          </Notes>
        </Slide>
        <Slide>
          <Heading>
            Demo
          </Heading>
          <Text>
            <Link href="https://markbook.org">Markbook - Social Bookmarks</Link>
          </Text>
          <Text>
            <Link href="https://pixolid.netlify.com/">Pixolid - Foto sharing</Link>
          </Text>
          <Notes>
            Live Demo von Markbook und Pixolid.
          </Notes>
        </Slide>
        <Slide align="flex-start center" bgRepeat="no-repeat" bgSize="80%" bgImage="./images/break-up-facebook-1.svg">
          <Notes>
            Zentrale Platformen wie Facebook werden natürlich einen Teufel tun, kompatibel zu Solid zu werden. Dies liegt
            überhaupt nicht in ihrem Interessen. Um Facebook lässt sich nur noch politisch bändigen und die Rufe Facebook
            zu zerschlagen sind ja in letzter Zeit recht präsent.
          </Notes>
        </Slide>
        <Slide align="flex-start center" bgRepeat="no-repeat" bgSize="80%" bgImage="./images/break-up-facebook-2.svg">
          <Notes>
            Der sinnvollste Weg dies zu realisieren ist meiner Meinung nach Facebook den Datenprovider und die Facebook Apps
            zu trennen.
          </Notes>
        </Slide>
        <Slide align="flex-start center" bgRepeat="no-repeat" bgSize="80%" bgImage="./images/break-up-facebook.svg">
          <Notes>
            Zwischen beiden Bereichen sollten offene Standards erzwungen werden. Dies könnten z.B. genau die Solid-Web-Standards sein.
            Nutzer:innen haben dann die Möglichkeit mit ihren Daten zu einem anderen Provider zu wechseln und trotzdem noch
            Facebook Apps zu nutzen. Oder Sie können Ihre Daten bei Facebook lassen, aber andere Apps benutzen.
          </Notes>
        </Slide>
        <Slide>
          <Heading>
            Los geht's!
          </Heading>
          <List>
            <ListItem>Hol dir einen <Link href="https://solid.community/register">Solid POD</Link></ListItem>
            <ListItem>Bau eine App - alles ist möglich!</ListItem>
            <ListItem>Bringe Open Source Software ins Solid Web</ListItem>
            <ListItem>Implementiere die Standards</ListItem>
          </List>
          <Notes>
            Was können wir also nun tun um das Social Web zurück zu erobern?
            <ListItem>POD selbst hosten oder erstmal solid.community</ListItem>
            <ListItem>Permissionless Innovation. Solid kann alles speichern nicht nur klassisches Social Media</ListItem>
            <ListItem>Wenn ihr Open Source macht: Macht es Solid kompatibel</ListItem>
            <ListItem>Implement Solid Standards in bestehende oder neue Webserver</ListItem>
            <ListItem>Wir brauchen viele Entwickler:innen und besseres UX/UI</ListItem>
          </Notes>
        </Slide>
        <Slide>
          <Heading size={1}>Werde Teil der Solid Community</Heading>
          <Image src="./images/community.svg"/>
          <List>
            <ListItem><URL href="https://solid.inrupt.com" /></ListItem>
            <ListItem><URL href="https://forum.solidproject.org" /></ListItem>
            <ListItem><URL href="https://gitter.im/solid/" /></ListItem>
            <ListItem><URL href="https://github.com/solid" /></ListItem>
          </List>
          <Notes>
            Es gibt zahlreiche Möglichkeiten sich in der Community einzubringen.
            Für erste Infos bietet sich das Developer Portal bei Inrupt an.
            Zum persönlichen Austausch haben wir ein Discourse Forum und diverse Gitter Chatrooms.
            Die Spec, etliche weitere Informationen und natürlich Code findet ihr auf Github.
          </Notes>
        </Slide>
        <Slide>
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            Die Rückeroberung des
          </Heading>
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            Social Web
          </Heading>
          <Heading caps fit>Fragen?</Heading>
          <Heading textColor="quaternary" caps fit>Diskussion!</Heading>
          <Text textSize={32}><URL href="https://angelo.veltens.org" /></Text>
        </Slide>
      </Deck>
    );
  }
}
