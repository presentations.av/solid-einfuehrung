## Slides

  * [Web version](https://angelo.veltens.org/slides/2019/solid-einfuehrung/)
  * [PDF version](https://angelo.veltens.org/slides/2019/solid-einfuehrung.pdf)

## Attributions / Licenses

The following images are licensed separately:

 * social_media.jpg: <a href="https://www.flickr.com/photos/nickrate/">nickrate</a> - <a href="https://creativecommons.org/licenses/by/2.0/">cc-by 2.0</a>
 * facebook.jpg: <a href="https://pixabay.com/de/users/geralt-9301/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1903445">Gerd Altmann</a> auf <a href="https://pixabay.com/de/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1903445">Pixabay</a>
 * platforms.jpg: <a href="https://pixabay.com/de/users/MikeRenpening-1178818/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1138001">Mike Renpening</a> auf <a href="https://pixabay.com/de/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1138001">Pixabay</a>
 * hell.jpg: <a href="https://pixabay.com/de/users/kalhh-86169/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1105352">kalhh</a> auf <a href="https://pixabay.com/de/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1105352">Pixabay</a>
 * trapped.jpg: <a href="https://pixabay.com/de/users/MichaelGaida-652234/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1670222">Michael Gaida</a> auf <a href="https://pixabay.com/de/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1670222">Pixabay</a>
